<html>
    <head>
        <script>
            var number = Math.floor((Math.random() * 100) + 1);
            var numGuesses = 0;
            var guessTable = document.getElementById("prevGuesses");

            function canGuess(){
                var guess = document.getElementById("guess").value;

                if(isNaN(guess) == true || guess < 1 || guess > 100){
                    document.getElementById("validEntry").innerHTML = "Please enter a number between 1-100.";
                }
                else{
                    document.getElementById("validEntry").innerHTML = "";

                    runGuess();
                }
            }

            function runGuess(){
                numGuesses++;

                var newRow = document.createElement('tr');
                var guess = document.getElementById("guess").value;

                if(guess > number){             
                    newRow.innerHTML = "<td>Guess #" + numGuesses + ":</td><td>" + guess + "</td><td>is too high.</td>";
                    document.getElementById("prevGuesses").appendChild(newRow);
                }
                if(guess < number){             
                    newRow.innerHTML = "<td>Guess #" + numGuesses + ":</td><td>" + guess + "</td><td>is too low.</td>";
                    document.getElementById("prevGuesses").appendChild(newRow);
                }
                if(guess == number){             
                    newRow.innerHTML = "<td>Guess #" + numGuesses + ":</td><td>" + guess + "</td><td>is correct.</td>";
                    document.getElementById("prevGuesses").appendChild(newRow);
                    document.getElementById("guessButton").style.display = "none";
                    document.getElementById("postButton").style.display = "block";
                    document.getElementById("guess").outerHTML = "<p>That is correct.</p>";
                }
                
            } 

            function postScore(){
                var user = document.getElementById("user").innerHTML;

                document.getElementById("forcePost").innerHTML = 
                    "<form id=\"postScore\" action=\"/~gm30452/assignment1/postScore.php\" method=\"post\">" +
                    "<input type=\"text\" name=\"username\" value=\"" + user + "\">" +
                    "<input type=\"text\" name=\"score\" value=\"" + numGuesses + "\">" +
                    "</form>";

                    document.getElementById("postScore").submit();
            }
        </script>
    </head>
    <body>
        <p>User: </p><p id="user"><?php echo $_POST['username'] ?></p>
        
        <p>Guess a number between 1-100</p>

        <form>
            <input type="text" id="guess">
        </form>

        <p id="validEntry"></p>

        <button id="guessButton" onclick="canGuess()">Guess</button>
        <button id="postButton" onclick="postScore()" style="display: none;">Post Score</button>
        
        <table id="prevGuesses"></table>

        <p>High Scores</p>

        <table id="highScores">
        <?php
            $servername = "localhost";
            $username = "W01130452";
            $password = "Garrickcs!";
            $database = "W01130452";
    
            $conn = new mysqli($servername, $username, $password, $database);
    
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM gameHighScores ORDER BY score ASC LIMIT 10";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    echo "<tr><td>".$row["username"]." ".$row["score"]."</td></tr>";
                }
                echo "</table>";
            }
        ?>
        </table>

        <p id="forcePost"></p>
    </body>
</html>