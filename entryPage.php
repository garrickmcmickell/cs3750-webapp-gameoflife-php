<html>
    <head>
        <script src = "http://icarus.cs.weber.edu/~gm30452/assignment1/hashAlgorithm.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>   
        <script>   
                function tryLogin(){
                    var hash = hashPass(document.getElementById("signInUsername").value, document.getElementById("signInPassword").value);

                    document.getElementById("forcePost").innerHTML = 
                    "<form id=\"loginForm\" action=\"/~gm30452/assignment1/tryLogin.php\" method=\"post\">" +
                    "<input type=\"text\" name=\"username\" value=\"" + document.getElementById("signInUsername").value + "\">" +
                    "<input type=\"password\" name=\"password\" value=\"" + hash + "\">" +
                    "</form>";
                    
                    document.getElementById("loginForm").submit();
                }

                function trySignUp(){
                    var hash = hashPass(document.getElementById("signUpUsername").value, document.getElementById("signUpPassword").value);

                    document.getElementById("forcePost").innerHTML = 
                    "<form id=\"signUpForm\" action=\"/~gm30452/assignment1/trySignUp.php\" method=\"post\">" +
                    "<input type=\"text\" name=\"username\" value=\"" + document.getElementById("signUpUsername").value + "\">" +
                    "<input type=\"password\" name=\"password\" value=\"" + hash + "\">" +
                    "</form>";

                    document.getElementById("signUpForm").submit();
                }

                function hashPass(user, pass){
                    var hashPass = user + ":" + pass;
                    return Sha256.hash(hashPass);
                }
        </script>  
    </head>
    <body>
        <p>Login</p>

        <form>
            <input type="text" id="signInUsername" value="Username">
            <input type="password" id="signInPassword" value="Password">
        </form>

        <button id="login" onclick="tryLogin()">Login</button>

        <?php
            if(isset($_POST['errorLogin']) == true){
                echo "<p>" . $_POST['errorLogin'] . "</p>";
            }
        ?>

        <p>Sign Up</p>

        <form>
            <input type="text" id="signUpUsername" value="Username">
            <input type="password" id="signUpPassword" value="Password">
        </form>

        <button id="signUp" onclick="trySignUp()">Sign Up</button>

        <?php
            if(isset($_POST['errorSignUp']) == true){
                echo "<p>" . $_POST['errorSignUp'] . "</p>";
            }
        ?>

        <p>Continue as Guest</p>

        <form action="/~gm30452/assignment1/tryGuest.php" method="post">
            <input type="text" id="guestUsername" name="username" value="Username">
            <input type="Submit" value="Continue">
        </form>

        <?php
            if(isset($_POST['errorGuest']) == true){
                echo "<p>" . $_POST['errorGuest'] . "</p>";
            }
        ?>

        <p id="forcePost"></p>
    </body>
</html>